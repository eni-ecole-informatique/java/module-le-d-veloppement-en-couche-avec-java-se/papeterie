package fr.eni.papeterie.dal;

/**
 * Classe permettant d'obtenir une instance d'ArticleDAOJdbcImpl et de permttre d'oter la dépendance forte
 * entre la couche BLL et la couche DAL. On l'instancie ici pour ne pas avoir à le faire dans la couche BLL.
 * Ainsi en cas de changement de technologie, nous n'avons plus qu'à recetter la couche BLL.
 * @author Enrick Enet
 */
public class DAOFactory {

    //Nous encapsulons la creation d'instance dans une nouvelle classe DAOFactory
    //cette classe comporte une methode static getArticleDAO() qui renvoie le type ArticleDAO et non le type
    //réel de l'instance.
    //le fait que la méthode soit 'static' facilite l'appel à la méthode, puisqu'aucune instance de DAOFactory
    //n'est alors necessaire pour l'utiliser
    public static ArticleDAO getArticleDAO() throws  DALException {
        ArticleDAO articleDAO = null;

        try {
            articleDAO = (ArticleDAO) Class.forName("fr.eni.papeterie.dal.jdbc.ArticleDAOJdbcImpl").newInstance();
        } catch (InstantiationException e) {
            throw new DALException("Problème d'instanciation ",e);
        } catch (IllegalAccessException e) {
            throw new DALException("Erreur d'accès ", e);
        } catch (ClassNotFoundException e) {
            throw new DALException("Classe non trouvée ", e);
        }

        return articleDAO;
    }
}
