package fr.eni.papeterie.dal;

/**
 * Classe qui permet de gérer les différentes Exceptions de la couche DAL.
 * @author Enrick Enet
 */
public class DALException extends Throwable {

    //Constructeurs :
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur par défaut.
     */
    public DALException() {
    }

    /**
     * Constructeur surchargé qui prend en paramètre un message.
     * @param message - le message d'erreur à afficher.
     */
    public DALException(String message) {
        super(message);
    }

    /**
     * Constructeur surchargé qui prend en paramètre un message et une cause.
     * @param message - le message d'erreur à afficher.
     * @param cause - la cause de l'erreur.
     */
    public DALException(String message, Throwable cause) {
        super(message, cause);
    }

    //Méthodes :
    @Override
    public String getMessage() {
        StringBuffer sb = new StringBuffer("Couche DAL - ");
        sb.append(super.getMessage());
        return sb.toString();
    }
}
