package fr.eni.papeterie.dal;

import fr.eni.papeterie.bo.Article;

import java.util.List;

public interface ArticleDAO {

    //Sélectionner un article par son idArticle.
    Article selectionnerParID(int idArticle) throws DALException;

    //Sélectionner tous les articles.
    List<Article> lister() throws DALException;

    //Modifier les attributs d'un article connu en BDD.
    void modifier(Article article) throws DALException;

    //Insertion d'un article en BDD.
    void inserer(Article article) throws DALException;

    //Supprimer un article
    void supprimer(int idArticle) throws DALException;

    //Sélectionner les articles par marque.
    List<Article> selectionnerParMarque(String marque) throws DALException;

    //Sélectionner les articles par mot cle.
    List<Article> selectionenrParMotCle(String motCle) throws DALException;
}
