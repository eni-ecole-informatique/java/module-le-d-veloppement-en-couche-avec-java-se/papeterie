package fr.eni.papeterie.dal.util;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.DriverManager;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import fr.eni.papeterie.dal.DALException;

/**
 * Classe DataBaseConnection gérant la connexion et la déconnexion à la BDD
 * @author Enrick Enet
 */
public class DataBaseConnection {
    //Déclaration des constantes :
    private static String
            CONST_URL,
            CONST_USER,
            CONST_PASSWORD;

    /**
     * Méthode permettant d'obtenir une connection.
     * @return - un objet de type Connection.
     * @throws DALException - une exception de type DALExeception
     */
    public static Connection seConnecter() throws DALException {
        Connection connexion = null;

        //Obtention d'une connexion :
        try {
            DriverManager.registerDriver(new SQLServerDriver());
            CONST_URL = Settings.getProperty("URL");
            CONST_USER = Settings.getProperty("USER");
            CONST_PASSWORD = Settings.getProperty("PASSWORD");
            connexion = DriverManager.getConnection(CONST_URL, CONST_USER, CONST_PASSWORD);
        } catch (SQLException e) {
            throw new DALException("Problème sur la chaine de connexion", e);
        }
        return connexion;
    }

    /**
     * Méthode qui permet de fermer la connexion ouverte.
     * @param connexion - objet de type Connection.
     * @throws DALException - une exception de type DALException.
     */
    public static void seDeconnecter(Connection connexion) throws DALException {
        if (connexion != null) {
            try {
                connexion.close();
            } catch (SQLException e) {
                throw new DALException("Problème de fermeture de connexion", e);
            }
        }
    }

    /**
     * Méthode qui permet de fermer la connexion ouverte et le statement.
     * Elle surcharge la méthode seDeconnecter().
     * @param statement - Objet de type Statement.
     * @param connexion - Objet de type Connexion.
     */
    public static void seDeconnecter(Statement statement, Connection connexion) throws DALException {
        try {
            if(statement != null){
                statement.close();
            }
        } catch (SQLException e) {
            throw new DALException("Problème de fermeture du statement", e);
        }
        seDeconnecter(connexion);
    }

    /**
     * Méthode qui permet de fermer la connexion ouverte et le preparedStatement.
     * Elle surcharge la méthode seDeconnecter().
     * @param preparedStatement - objet de type PreparedStatement.
     * @param connexion - objet de type Connexion.
     * @throws DALException - une eception de type DALException.
     */
    public static void seDeconnecter(PreparedStatement preparedStatement, Connection connexion) throws DALException {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            throw new DALException("Problème de fermeture du preparedStatement", e);
        }
        seDeconnecter(connexion);
    }
}
