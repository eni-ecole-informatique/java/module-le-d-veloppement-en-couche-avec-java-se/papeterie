package fr.eni.papeterie.dal.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Classe permettant le chargement des paramètres de connexion à la BDD par l'intermédiaire d'un fichier texte
 * avec extension .properties
 * @author Enrick Enet
 */
public class Settings {
    private static Properties properties;

    //Chargement du fichieren mémoire avant l'appel à la méthode getProperty
    static {
        try {
            properties = new Properties();
            // 1 - Accéder aux clés/valeurs d'un fichier texte avec extension properties
            properties.load(Settings.class.getResourceAsStream("connexion.properties"));

            // 2 - Ou acceder aux cles/valeurs d'un fichier xml
            //properties.loadFromXML(Settings.class.getResourceAsStream("connexion.xml"));
        //} catch (InvalidPropertiesFormatException e) {
        //	System.out.println("InvalidPropertiesFormat -  cause"+e.getCause());

        } catch (IOException e) {
            System.out.println("Cause : " + e.getCause());
        }
    }

    /**
     * Méthode qui permet d'obtenir une chaine de caractères correspondant à la clé renseignée en paramètre.
     * @param cle - Objet de type String.
     * @return - La valeur.
     */
    public static String getProperty(String cle) {
        String valeur = properties.getProperty(cle);
        return valeur;
    }
}
