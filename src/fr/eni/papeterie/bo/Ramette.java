package fr.eni.papeterie.bo;

/**
 * Classe qui permet de connaitre le détail d'une ramette.
 * Il sera caractérisé par son grammage.
 * @author Enrick Enet
 */

public class Ramette extends Article {
    //Attributs
    private int grammage;

    //Constructeur
    /**
     * Constructeur d'une Ramette par défaut sans parammètres.
     * On utilise ici un constructeur surchargé de la classe mère(Article)
     * en chargeant les attributs à null et zéro.
     */
    public Ramette(){
        /* Constructeur surchargé */
        super(null, null, null, 0, 0);
    }

    /**
     * Constructeur d'une ramette qui prend en paramètres :
     * @param marque - la marque de la ramette.
     * @param reference - la référence de la ramette.
     * @param designation - la désignation de la ramette.
     * @param prixUnitaire - le prix unitaire de la ramette.
     * @param qteStock- la qunatité de ramette(s) en stock.
     * @param grammage - le grammage de la ramette.
     */
    public Ramette(String marque, String reference,  String designation, float prixUnitaire, int qteStock, int grammage) {
        super( marque, reference, designation, prixUnitaire , qteStock);
        setGrammage(grammage);
    }

    /**
     * Constructeur d'une ramette qui prend en paramètres :
     * @param idArticle - l'identifiant de la ramette.
     * @param marque - la marque de la ramette.
     * @param reference - la référence de la ramette.
     * @param designation - la désignation de la ramette.
     * @param prixUnitaire - le prix unitaire de la ramette.
     * @param qteStock- la qunatité de ramette(s) en stock.
     * @param grammage - le grammage de la ramette.
     */
    public Ramette(Integer idArticle, String marque, String reference,  String designation,
                   float prixUnitaire, int qteStock,  int grammage) {
        super(idArticle, marque, reference, designation, prixUnitaire, qteStock);
        setGrammage(grammage);
    }

    //Accesseurs et mutateurs
    /**
     * Affiche le grammage de la ramette.
     * @return - le grammage de la ramette.
     */
    public int getGrammage() { return grammage; }

    /**
     * Modifie le grammage de la ramette.
     */
    public void setGrammage(int grammage) { this.grammage = grammage; }


    //Autres méthodes
    /**
     * Méthode qui permet d'afficher une ramette sous forme de
     * chaine de caractères.
     * @return le détail de la ramette.
     */
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(super.toString());
        buffer.append(" ");
        buffer.append("Ramette [grammage=");
        buffer.append(getGrammage());
        buffer.append("]");
        return buffer.toString();
    }
}
