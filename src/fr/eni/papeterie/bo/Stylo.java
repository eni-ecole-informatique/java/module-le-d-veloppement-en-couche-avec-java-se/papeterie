package fr.eni.papeterie.bo;

/**
 * Classe qui permet de connaitre le détail d'un article.
 * Il sera caractérisé par sa couleur.
 * @author Enrick Enet
 */

public class Stylo extends Article {
    //Attributs
    private String couleur;

    //Constructeur
    /**
     * Constructeur d'un Stylo par défaut sans parammètres.
     * Constructeur d'une Ramette par défaut sans parammètres.
     * On utilise ici un constructeur surchargé de la classe mère(Article)
     * en chargeant les attributs à null et zéro.
     */
    public Stylo() {
        /* Constructeur surchargé */
        super(null, null, null, 0, 0);
    }

    /**
     * Constructeur d'un Stylo qui prend en paramètres :
     * @param marque - la marque du stylo.
     * @param reference - la référence du stylo.
     * @param designation - la désignation du stylo.
     * @param prixUnitaire - le prix unitaire du stylo.
     * @param qteStock - la quantité de stylo(s) en stock.
     * @param couleur - la couleur du stylo.
     */
    public Stylo(String marque, String reference, String designation,
                 float prixUnitaire, int qteStock, String couleur) {
        super(marque, reference, designation, prixUnitaire, qteStock);
        setCouleur(couleur);
    }

    /**
     * Constructeur d'un Stylo qui prend en paramètres :
     * @param idArticle - l'identifiant du stylo.
     * @param marque - la marque du stylo.
     * @param reference - la référence du stylo.
     * @param designation - la désignation du stylo.
     * @param prixUnitaire - le prix unitaire du stylo.
     * @param qteStock - la quantité de stylo(s) en stock.
     * @param couleur - la couleur du stylo.
     */
    public Stylo(Integer idArticle, String marque, String reference,  String designation,
                 float prixUnitaire, int qteStock, String couleur) {
        super(idArticle, marque, reference,  designation, prixUnitaire,  qteStock);
        setCouleur(couleur);
    }

    //Accesseurs et mutateurs
    /**
     * Affiche la couleur d'un stylo.
     * @return - la couleur du stylo.
     */
    public String getCouleur() { return couleur; }

    /**
     * Modifie la couleur d'un stylo.
     */
    public void setCouleur(String couleur) { this.couleur = couleur; }

    //Autres méthodes
    /**
     * Méthode qui permet d'afficher le détail d'un stylo sous forme de
     * chaine de caractères.
     * @return le détail du stylo.
     */
    @Override
    public String toString() {
        String s = String.format("%s Stylo [Couleur=%s]", super.toString(), getCouleur());
        return s;
    }
}
