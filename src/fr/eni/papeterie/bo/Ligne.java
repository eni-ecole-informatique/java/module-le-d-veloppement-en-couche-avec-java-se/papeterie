package fr.eni.papeterie.bo;

/**
 * Classe qui permet de regrouper sur une ligne les caracteristiques d'un article
 * ainsi que le montant de l'article.
 * @author Enrick Enet
 */

public class Ligne {
    //Attributs
    protected int quantite;
    private Article article; //Association

    //Constructeur
    /**
     * Constructeur d'une ligne qui prend en paramètre une quantité et un Article.
     * @param quantite - la quantité de ligne(s).
     * @param article - l'article à associer.
     */
    public Ligne(Article article, int quantite) {
        setQuantite(quantite);
        setArticle(article);
    }

    //Accesseurs et mutateurs
    /**
     * Affiche la quantité de ligne(s).
     * @return - la quantité de ligne(s).
     */
    public int getQuantite() { return quantite; }

    /**
     * Modifie la quantité de ligne(s).
     */
    public void setQuantite(int quantite) { this.quantite = quantite; }

    /**
     * Affiche l'article de la ligne.
     * @return - l'article de la ligne.
     */
    public Article getArticle() { return article; }

    /**
     * Modifie l'article de la ligne.
     */
    private void setArticle(Article article) { this.article = article; }

    public float getPrix()
    {
        return article.getPrixUnitaire();
    }


    //Autres méthodes
    /**
     * Méthode qui permet d'afficher le détail d'une ligne sous forme de
     * chaine de caractères.
     * @return le détail de la ligne.
     */
    public String toString()
    {
        StringBuffer buf = new StringBuffer();
        buf.append("Ligne [");
        buf.append(" qte=");
        buf.append(getQuantite());
        buf.append(", prix=");
        buf.append(getPrix());
        buf.append(", ");
        buf.append("article=");
        buf.append((article!=null)?getArticle().toString():"<< non renseign� >>");// par exp ternair
/*
		if (article != null) {
			buf.append("article=");
			buf.append(getArticle().toString());
		}
*/
        buf.append("]");
        return buf.toString();
    }

}
