package fr.eni.papeterie.bo;

/**
 * Classe qui permet d'afficher le détail d'un panier.
 * Il sera carctérisé par un montant et une liste d'articles.
 * @author Enrick Enet
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet de connaitre le détail de la commande
 * Il sera caractérisé par
 * @author Enrick Enet
 */
public class Panier {
    //Attributs
    private float montant;
    private List<Ligne> lignesPanier;

    /**
     * Constructeur par défaut
     */
    public Panier() {
        /* Penser à instancier la lignePanier */
        lignesPanier = new ArrayList<Ligne>();
    }

    //Mutateurs et accesseurs
    /**
     * Permet d'afficher le montant du panier.
     * @return - le montant.
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Permet de modifier le montant du panier.
     */
    public void setMontant(float montant) { this.montant = montant; }

    /**
     * Permet de sélectionner une ligne du panier.
     * @param index - détermine l'index.
     * @return - retourne la ligne sélectionnée.
     */
    public Ligne getLigne(int index ) {
        return lignesPanier.get(index);
    }

    /**
     * Permet d'afficher la liste des lignes du panier.
     * @return - retourne l'ensemble des lignes du panier.
     */
    public List<Ligne> getLignesPanier() {
        return lignesPanier;
    }

    //Autres Méthodes
    /**
     * Permet d'ajouter une ligne d'article dans le panier.
     * @param article - l'article à ajouter.
     * @param quantite - la quantité à ajouter.
     */
    public void addLigne(Article article, int quantite) {
        /* Création d'une nouvelle ligne */
        Ligne ligneAdding = new Ligne(article, quantite);
        lignesPanier.add(ligneAdding);
    }

    /**
     * Permet de modifier une quantité sur une ligne du panier.
     * @param index - détermine la position dans la liste à modifier.
     * @param newQte - la nouvelle quantité à enregistrer.
     */
    public void updateLigne(int index, int newQte) {
        lignesPanier.get(index).setQuantite(newQte);
    }

    /**
     * Permet de supprimer une ligne dans la liste du panier.
     * @param index - détermine la position dans la liste à supprimer.
     */
    public void removeLigne(int index) {
        lignesPanier.remove(index);
    }

    /**
     * Méthode qui utilise un StringBuffer permettant de formater une chaine de caracteres.
     * définis pour l'affichage du panier.
     * @return le contenu du panier.
     */
    @Override
    public String toString() {
        setMontant(0f);
        StringBuffer bf =new StringBuffer();
        bf.append("Panier : \n\n");
        for (Ligne ligne : lignesPanier) {
            if (ligne != null){
                setMontant(getMontant()+(ligne.getPrix()*ligne.getQuantite()));

                bf.append("ligne " + lignesPanier.indexOf(ligne) + " :\t");
                bf.append(ligne.toString());
                bf.append("\n");
            } else break;
        }
        bf.append("\nValeur du panier : " + getMontant());
        bf.append("\n\n");
        return bf.toString();
    }
}
