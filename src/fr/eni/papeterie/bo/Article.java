package fr.eni.papeterie.bo;

/**
 * Classe qui permet de connaitre les caractéristiques d'un article.
 * Il sera caractérisé par un identifiant, une référence, une marque, une désignation,
 * une quantité en stock et un prix unitaire.
 * @author Enrick Enet
 */

public abstract class Article {
    //Attributs
    private String reference, marque, designation;
    private Integer idArticle;
    private int qteStock;
    private float prixUnitaire;

    //Constructeurs
    /**
     * Constructeur d'un Article qui prend en paramètres :
     * @param marque - la marque de l'article.
     * @param reference - La référence de l'article.
     * @param designation - la désignation de l'article.
     * @param prixUnitaire - le prix unitaire de l'article.
     * @param qteStock - la quantité d'article.
     */
    public Article(String marque, String reference,  String designation, float prixUnitaire, int qteStock) {
        setReference(reference);
        setMarque(marque);
        setDesignation(designation);
        setQteStock(qteStock);
        setPrixUnitaire(prixUnitaire);
    }

    /**
     * Constructeur d'un Article qui prend en paramètres :
     * @param idArticle - l'identifiant de l'article.
     * @param marque - la marque de l'article.
     * @param reference - La référence de l'article.
     * @param designation - la désignation de l'article.
     * @param prixUnitaire - le prix unitaire de l'article.
     * @param qteStock - la quantité d'article.
     */
    public Article(Integer idArticle, String marque, String reference, String designation, float prixUnitaire, int qteStock ) {
        this(reference, marque, designation,prixUnitaire, qteStock);
        setIdArticle(idArticle);
    }

    //Accesseurs et mutateurs
    /**
     * Affiche la référence de l'article.
     * @return - la référence de l'article.
     */
    public String getReference() { return reference; }

    /**
     * Modifie la référence de l'article.
     * @param reference - la référence de l'article.
     */
    public void setReference(String reference) { this.reference = reference; }

    /**
     * Affiche la marque de l'article.
     * @return - la marque de l'article.
     */
    public String getMarque() { return marque; }

    /**
     * Modifie la marque de l'article.
     */
    public void setMarque(String marque) { this.marque = marque; }

    /**
     * Affiche la désignation de l'article.
     * @return - la désignation de l'article.
     */
    public String getDesignation() { return designation; }

    /**
     * Modifie la désignation de l'article.
     */
    public void setDesignation(String designation) { this.designation = designation; }

    /**
     * Affiche l'identifiant de l'article.
     * @return - l'identifiant de l'article.
     */
    public Integer getIdArticle() { return idArticle; }

    /**
     * Modifie l'identifiant de l'article.
     */
    public void setIdArticle(Integer idArticle) { this.idArticle = idArticle; }

    /**
     * Affiche la quantité d'article(s) en stock.
     * @return - la quantité d'article(s) en stock.
     */
    public int getQteStock() { return qteStock; }

    /**
     * Modifie la quantité d'article(s) en stock.
     */
    public void setQteStock(int qteStock) { this.qteStock = qteStock; }

    /**
     * Affiche le prix unitaire de l'article.
     * @return - le prix unitaire de l'article.
     */
    public float getPrixUnitaire() { return prixUnitaire; }

    /**
     * Modifie le prix unitaire de l'article.
     */
    public void setPrixUnitaire(float prixUnitaire) { this.prixUnitaire = prixUnitaire; }

    //Autres méthodes
    /**
     * Méthode qui permet d'afficher le détail d'un article sous forme de
     * chaine de caractères.
     * @return le détail d'un article.
     */
    @Override
    public String toString() {
        return "Article [idArticle="+ getIdArticle() +", marque=" + getMarque() +", reference=" + getReference() +  ", designation=" + getDesignation()
                + ", prixUnitaire=" + getPrixUnitaire() + ", qteStock=" + getQteStock() + "]";
    }
}
