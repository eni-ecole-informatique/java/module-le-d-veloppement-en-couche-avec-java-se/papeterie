create procedure modifierArticle @reference nchar(10), @marque nvarchar(200), @designation nvarchar(250), @prixUnitaire float, @qteStock int, @grammage int, @couleur nvarchar(50), @idArticle int
as
UPDATE articles SET reference=@reference,marque=@marque,designation=@designation,prixUnitaire=@prixUnitaire,qteStock=@qteStock,grammage=@grammage,couleur=@couleur 
WHERE idArticle=@idArticle