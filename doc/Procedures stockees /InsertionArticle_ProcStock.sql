create procedure insererArticle @reference nchar(10), @marque nvarchar(200), @designation nvarchar(250), @prixUnitaire float, @qteStock int, @grammage int, @couleur nvarchar(50), @type nchar(10)
as
begin
declare @nombre int;
SELECT @nombre=MAX(idArticle+1) FROM Articles;

SET IDENTITY_INSERT articles ON
INSERT INTO articles (idArticle,reference,marque,designation,prixUnitaire,qteStock,grammage,couleur,type) VALUES(@nombre,@reference,@marque,@designation,@prixUnitaire,@qteStock,@grammage,@couleur,@type);
SET IDENTITY_INSERT articles OFF
end