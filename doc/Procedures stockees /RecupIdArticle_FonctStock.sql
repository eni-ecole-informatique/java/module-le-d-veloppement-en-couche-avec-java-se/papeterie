CREATE function recupIdArticle() RETURNS int AS
begin
declare @nombre int;
select @nombre=max(idArticle) from articles;
return @nombre;
end